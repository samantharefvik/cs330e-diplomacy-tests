#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_solve, diplomacy_eval, diplomacy_print

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # eval
    # ----

    def test_eval_1(self):
        s = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Support', 'B']]
        i = diplomacy_eval(s)
        self.assertEqual(i, ['A [dead]', 'B Madrid', 'C London'])

    def test_eval_2(self):
        s = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid']]
        i = diplomacy_eval(s)
        self.assertEqual(i, ['A [dead]', 'B [dead]'])

    # ----
    # eval
    # ----

    def test_print_1(self):
        s = ['A [dead]', 'B Madrid', 'C London']
        w = StringIO()
        diplomacy_print(s, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_print_2(self):
        s = ['A [dead]', 'B [dead]']
        w = StringIO()
        diplomacy_print(s, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    # ----
    # solve (all piazza test cases)
    # ----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")


    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Support C\nE Dallas Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC London\nD Austin\nE Dallas\n")


    def test_solve_3(self):
        r = StringIO("A Madrid Support C\nB Barcelona Hold\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Barcelona\nC [dead]\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Support A\nC Austin Support B\nD London Move Barcelona\nE Dallas Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Barcelona\nC Austin\nD [dead]\nE [dead]\n")


    def test_solve_5(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")


    def test_solve_6(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")


    def test_solve_7(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")


    def test_solve_8(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")


    def test_solve_9(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")


    def test_solve_10(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")


    def test_solve_11(self):
        r = StringIO("A Madrid Hold\nB Barcelona Support A\nC London Support B\nD Paris Move Barcelona\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Barcelona\nC London\nD [dead]\n")


    def test_solve_12(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\nF NewYork Move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\nE [dead]\nF [dead]\n")


    def test_solve_13(self):
        r = StringIO("A Barcelona Move Madrid\nB Madrid Hold\nC NewYork Support D\nD London Move Barcelona\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC NewYork\nD Barcelona\n")


# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


% cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestDiplomacy.out



% cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
